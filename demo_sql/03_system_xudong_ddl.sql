create table USER_INFO
(
  id       NUMBER,
  username VARCHAR2(200),
  email    VARCHAR2(200),
  gender   NUMBER(1)
)