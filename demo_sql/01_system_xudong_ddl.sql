create table student (
       id number,
       name varchar2(200),
       age number(3),
       address varchar2(200)
);
create sequence id_seq
start with 1
INCREMENT BY 1;